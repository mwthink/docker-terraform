FROM hashicorp/terraform:0.11.13

ENV ARCH=amd64
ENV K8S_VERSION=v1.14.0
ENV K8S_BIN_URL=https://storage.googleapis.com/kubernetes-release/release/${K8S_VERSION}/bin/linux/${ARCH}
ENV HELM_VERSION=v2.13.1
ENV HELM_RELEASE_TAR=https://storage.googleapis.com/kubernetes-helm/helm-${HELM_VERSION}-linux-${ARCH}.tar.gz

# install kubectl
RUN cd /usr/bin && curl -sLO ${K8S_BIN_URL}/kubelet

# Install helm
RUN \
  curl -s $HELM_RELEASE_TAR | tar xz && \
  mv ./linux-amd64/helm /usr/bin/ && \
  rm -rf ./linux-amd64
